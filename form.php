<!DOCTYPE html>
<html lang="ru">


	<head> <!-- Техническая информация о документе -->
		<meta charset="UTF-8"> <!-- Определяем кодировку символов документа -->
		<title>P O M O G I T E</title> <!-- Задаем заголовок документа -->
		<link rel="stylesheet" href="style.css">
	</head>

	<body>
		<header><div>
  			<img src="https://i.imgur.com/UxHIhiw.png" width="100"  height = "100" style="float:left; margin-right: 10px" alt="Moon">
			P O M O G I T E</div>

</header>
<nav>
    <ul>
      <li><a href="#form" title = "Форма">Форма</a></li>
      <li>
        <?php 
        if(!empty($_COOKIE[session_name()]) && !empty($_SESSION['login']))
          print('<a href="./?quit=1" title = "Выйти">Выйти</a>');
        else
          print('<a href="login.php" title = "Войти">Войти</a>');
        ?>
      </li>
    </ul>
  </nav>

	<div id="main">
		<?php
	if (!empty($messages)) {
	print('<div id="messages">');
	// Выводим все сообщения.
	foreach ($messages as $message) {
		print($message);
	}
	print('</div>');
	}

	// Далее выводим форму отмечая элементы с ошибками классом error
	// и задавая начальные значения элементов ранее сохраненными.
	?>
	<div id = "formcontent">
	<h2 id="form" >Форма</h2>

   		<form action="." method ="POST" class="style">

   			Ваше имя:<br>
      		<label >
        	<input name="name" <?php if ($errors['name']) {print 'class="error"';} ?>
			value="<?php print $values['name']; ?>"/>
      		</label>
      		<br>

      		Ваш адрес электронной почты:<br>
      		<label >
        	<input name="email" <?php if ($errors['email']) {print 'class="error"';} ?>
			value="<?php print $values['email']; ?>"
          	type="email"/>
      		</label>
      		<br>

			Ваша дата рождения:<br>
      		<label>
			<input name="bd" <?php if ($errors['bd']) {print 'class="error"';} ?>
			value="<?php print $values['bd']; ?>"
          	type="date"/>
      		</label>
      		<br>

      		Ваша гендерная идентефикация:<br>
      		<label ><input type="radio" checked="checked"
        	name="sex"  value="M" <?php if ($values['sex'] == 'M') {print 'checked';} ?> />
        	Мужчина</label>
      		<label><input type="radio"
        	name="sex" value="F" <?php if ($values['sex'] == 'F') {print 'checked';} ?>  />
        	Женщина</label>
        	<label><input type="radio"
        	name="sex" value="O" <?php if ($values['sex'] == 'O') {print 'checked';} ?>/>
        	Небинарная радиокнопка</label>
        	<br>

			Количество конечностей:<br>
      		<label ><input type="radio" <?php if ($values['limbs'] == '2') {print 'checked';} ?>
			checked="checked" name="limbs"  value="2"  />
        	2</label>
      		<label><input type="radio" <?php if ($values['limbs'] == '3') {print 'checked';} ?>
        	name="limbs" value="3" />
        	3</label>
        	<label><input type="radio" <?php if ($values['limbs'] == '4') {print 'checked';} ?>
        	name="limbs" value="4" />
        	4</label>
        	<br>
			
			
			Сверхспособности:<br>
      		<label >
        	<select name="superpowers[]"
          	multiple="multiple">
         	<option value="1" <?php if ($values['superpowers']['0']) {print 'selected';} ?>>Бессмертие</option>
          	<option value="2" <?php if ($values['superpowers']['1']) {print 'selected';} ?>>Прохождение сквозь стены</option>
          	<option value="3" <?php if ($values['superpowers']['2']) {print 'selected';} ?>>Левитация</option>
        	</select>
      		</label>
      		<br>

      		Биография:<br>
      		<label>
        	<textarea name="bio">Всё хорошо, что хорошо кончается</textarea>
      		</label>
      		<br>

      		<br>
      		<label <?php if ($errors['contract']) {print 'class="error"';} ?>><input type="checkbox" checked="checked"
        	name="contract" />
        	С контрактом ознакомлен</label>
        	<br>
 
      <input type="submit" value="Отправить" />
    </form>
	</div>
    </div>
   
    <footer> Мастерами кунг-фу - не рождаются. Мастерами кунг-фу - становятся </footer> 
	</body>

</html>
